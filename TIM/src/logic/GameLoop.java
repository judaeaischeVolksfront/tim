package logic;

import java.awt.Canvas;



import gui.Game;

/**
 * @author Daniel
 * @version 1.0
 * @date 16.11.2015 Bei dieser Klasse handelt es sich um die Game Loop. Sie h�lt
 *       das Spiel am laufen, aktualisiert das spiel und sorgt daf�r dass das
 *       Spiel auf allen PC's gleich fl��ig l�uft.
 */
public class GameLoop extends Canvas implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean running = false;
	private Thread thread;
	Game game = new Game();
	
	Hero helene; //damit der Held getickt wird
	Weapon sword;
	Controller gamepad;
	int counter = 0;
	

	// Methode zum starten der Game Loop
	public synchronized void start() {
		// F�hrt die Methode nur aus wenn der Thread noch nicht l�uft
		if (running) 
			return;
		
		running = true;
		// F�gt die Game Loop dem Thread hinzu und startet diesen
		thread = new Thread(this);
		thread.start();
	}

	// Methode zum beenden der Game Loop
	private synchronized void stop() {
		// F�hrt die Methode nur aus wenn es einen laufenden Thread gibt
		if (!running) 
			return;
		
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(1);
	}

	// Die Game Loop Methode an sich
	@Override
	public void run() {
		game.init();
		
		long lastTime = System.nanoTime();
		final double AMOUNT_OF_TICKS = 60.0;
		double ns = 1000000000 / AMOUNT_OF_TICKS;
		double delta = 0;
		int updates = 0;
		int frames = 0;
		long timer = System.currentTimeMillis();
		helene =  game.getHero(); //tick Held auf initialisierten stellen
		gamepad = game.getController();
		sword = game.getWeapon();
		// Die eigentliche Loop
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			if (delta >= 1) {
				tick();
				updates++;
				delta--;
			}
			game.render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println(updates + " Ticks, FPS " + frames);
				updates = 0;
				frames = 0;
			}

		}
		stop();
	}

	
	private void tick() {
		game.requestFocus();
		helene.tick();
		gamepad.tick();
		if(game.getStatus() != ""){
			if(counter >= 180){ //Dauer der Statusanzeige
				game.setStatus("");
				counter = 0;
			}
			counter++;
		}
	}
	}


