package logic;
 
import gui.Game;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;

import data.Textures;
import data.Unit;

public class Door extends Unit {

	private Hero helene; 
	private Game game;
	private Textures tex;
	private LinkedList<Door> door = new LinkedList<Door>();
	
	public Door(double x,double y, Textures tex, Game game) {
		this.x = x;
		this.y = y;
		this.tex = tex;
		this.game = game;
		this.velX = 0;
		this.velY = 0;
		
		movementSpeed = 4;
		viewDirection = RIGHT;
		damage = 0;
		name = "Door";
		position = new Point(0,0);	
	}
	
	public void tick(){	
		
		doorCollision();
	}
	
	public void doorCollision(){
		
		if(Physics.collision(game.getDoor(), helene)){
			
		}
			this.game.changeRoom();
	}
	
	public void render(Graphics g) {
		
		g.drawImage(tex.door ,(int) x, (int) y, null);
	}
	
	public void movement(){
		
		x += velX;
		y += velY;
	}
	
	public LinkedList<Door> getDoor(){
		
		return door;
	}
}
