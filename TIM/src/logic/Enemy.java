package logic;


import java.awt.Graphics;
import java.awt.Point;

import data.Textures;
import data.Unit;
import gui.Game;

public class Enemy extends Unit {
	
	private Hero helene;
	private Textures tex;
	Controller con;
	Game game;

	
	public Enemy(double x, double y, Textures tex, Hero hero, Controller con, Game game){
		
		this.x = x;
		this.y = y;
		
		this.velX = 3;
		this.velY = 3;
		
		this.tex = tex;
		
		this.helene = hero;
		this.con=con;
		this.game = game;

		
		hp = 240;
		position = new Point(0,0);
		currentHp = hp;
		name = "Music";
		viewDirection = DOWN;
		damage = 1;
		movementSpeed = 2;
		height = 130;
		width = 100;

	}
	
	public void tick(){
		updatePosition();
		collisions();
		death();
	}
	
	public void updatePosition(){
		
		x+=velX;
		y+=velY;
	}
	
	public void collisions(){
		
		wallCollision();				
		heroCollision();		
	}
	
	public void death(){
		if(currentHp <= 0){
			con.removeEnemy(this);
			game.setStatus("Gegner wurde besiegt!");
		}
	}
	
	public void wallCollision(){

		//Left
		if(x<=80){
			x=80;
			velX *= -1;
		}
		
		//Right
		if(x>=1100){
			x=1100;
			velX *= -1;
		}
		
		//Down
		if(y<=50){
			y=50;
			velY *= -1;
		}
		
		//Top
		if(y>=490){
			y=490;
			velY *= -1;
		}
	}
	
	
	public void heroCollision(){

		if(Physics.collision(this, helene)){
			helene.setCurrentHp(helene.getCurrentHp() - damage);
		}
	}
	
	public void render(Graphics g){
		
		g.drawImage(tex.enemy, (int)x, (int)y, null);
	}
	
	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}
	
	public void setX(double x){
		this.x = x;	
	}
	
	public void setY(double y){
		this.y = y;	
	}
	
	public void setVelX(double velX){
		this.velX = velX;
	}
	
	public void setVelY(double velY){
		this.velY = velY;
	}
	
	public double getMovementspeed(){
		return movementSpeed;
	}
	
	public int getDamage(int damage){
		
		return damage;
	}


}
