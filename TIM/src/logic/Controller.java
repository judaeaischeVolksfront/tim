package logic;

import java.awt.Graphics;
import java.util.LinkedList;
import java.util.Random;

import data.Textures;
import gui.Game;

public class Controller {

	private LinkedList<Enemy> enemy = new LinkedList<Enemy>();
	private LinkedList<Door> doorList= new LinkedList<Door>();
	private LinkedList<Weapon> swordList = new LinkedList<Weapon>();
	private LinkedList<Hero> heleneList = new LinkedList<Hero>();
	
	Enemy tempEnemy;
	Game game;
	Textures tex;
	Hero helene;
	Door door;
	Weapon tempSword;
	int doorCount = 1;
	
	int enemyCount = 5;// maximale Anzahl der gegner
	int xPos, yPos;
	Random r;
	
	
	public Controller(Game game){
		this.game = game;
		this.tex = game.getTextures();
		this.helene = game.getHero();
		
		r = new Random();
		
		
		for(int i = 0; i < enemyCount; i++){ //enemyCount benutzen um gegner anzahl an raum anzupassen
			
			xPos = 100 + r.nextInt(900);
			yPos = 120 + r.nextInt(460);
			addEnemy(new Enemy(xPos, yPos, tex, helene, this, game));
		}
		
		// Added die Doors an gegebenen Positionen
		for(int i = 0; i < doorCount; i++){
			addDoor(new Door(1180,Game.HEIGHT / 2 + 50,tex,game));
		}
				

		addWeapon(new Weapon(helene.getX() - 80, helene.getY() + 70,tex.weaponL, game, helene));
		addWeapon(new Weapon(helene.getX() + 25, helene.getY() - 101,tex.weaponU, game, helene));
		addWeapon(new Weapon(helene.getX() + 25, helene.getY() + 101,tex.weaponD, game, helene));
		addWeapon(new Weapon(helene.getX() + 80, helene.getY() + 70,tex.weaponR, game, helene));
		
	}
	
	public void tick(){
		
		for(int i = 0; i < enemy.size(); i++){ // bei mehreren gegnern beide updaten
			tempEnemy = enemy.get(i);
			
			tempEnemy.tick();
		}
		
		for(int i = 0; i < doorList.size(); i++){ // bei mehreren t�ren beide updaten
			door = doorList.get(i);
			
//			door.tick();
		}
	
		for(int i=0;i<swordList.size();i++){
			tempSword = swordList.get(i);
			
			tempSword.tick();
		}
	}
	
	public void render(Graphics g){
		
		for(int i = 0; i < enemy.size(); i++){ // bei mehreren gegnern beide rendern
			tempEnemy = enemy.get(i);
			
			tempEnemy.render(g);
			
			
		}
		
		for(int i = 0; i < doorList.size(); i++){ // bei mehreren t�ren beide updaten
			door = doorList.get(i);
			
			door.render(g);
		}
	
		for(int i=0;i<swordList.size();i++){
			tempSword = swordList.get(i);
			
			tempSword.render(g);
		}
		for(int i=0;i<heleneList.size();i++){
			helene = heleneList.get(i);
			
			helene.render(g);
		}
	}
	
	public void switchLevel(LinkedList<Enemy> enemy,LinkedList<Hero> hero,LinkedList<Weapon> weapon, LinkedList<Door> door){
		int hpres = helene.getHp();
		for(int i = 1;i < weapon.size();i++){
		tempSword=weapon.get(i);
		removeWeapon(tempSword);
		}
		removeHero(helene);
		
		
		
		//switch(game.LEVEL)
		//{
		//case 1:
		//	break;
		//}
		//Top
		//tempHelene.setX(x);
		//tempHelene.setY(y);
		//Down
		//tempHelene.setX(x);
		//tempHelene.setY(y);
		//Left
		//tempHelene.setX(x);
		//tempHelene.setY(y);
		//Right
		//helene.setX(80);
		//helene.setY(Game.HEIGHT / 2 + 100);
		//addDoor(new Door(100,Game.HEIGHT / 2 - 50,tex,game));
	}	
	public void addEnemy(Enemy dummy){
		enemy.add(dummy);
	}
	
	public void removeEnemy(Enemy dummy){
		enemy.remove(dummy);
	}
	
	public LinkedList<Enemy> getEnemy(){
		return enemy;
	}
	
	//Door
	public void addDoor(Door doorDummy){
		doorList.add(doorDummy);
	}
	
	public void removeDoor(Door doorDummy){
		doorList.remove(doorDummy);
	}
	public LinkedList<Door> getDoor(){
		return doorList;
	}
	
	public void addWeapon(Weapon dummy){
		swordList.add(dummy);
	}
	public void removeWeapon(Weapon dummy){
		swordList.remove(dummy);
	}
	
	public LinkedList<Weapon> getWeapon(){
		return swordList;
	}
	
	public void removeHero(Hero dummy){
		heleneList.remove(dummy);
	}
	public void addHero(Hero dummy){
		heleneList.add(dummy);
	}
}
