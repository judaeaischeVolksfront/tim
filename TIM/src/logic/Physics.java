package logic;

import java.util.LinkedList;

public class Physics {

	public static boolean collision(Hero helene, LinkedList<Enemy> music) {// �berpr�ft ob hero einen der gegner trifft

		for (int i = 0; i < music.size(); i++) {// geht die LinkedList Enemy durch
			if (helene.getBounds(100, 130).intersects(music.get(i).getBounds(100, 130))) 
				return true;
		}

		return false;
	}

	public static boolean collision(Weapon sword, LinkedList<Enemy> music) {
		for (int i = 0; i < music.size(); i++) {
			if (sword.getBounds(100, 50).intersects(music.get(i).getBounds(100, 130))) {
				
				return true;
			}
		}
		return false;
	}
	
	public static int collisionID(Weapon sword, LinkedList<Enemy> music) {
		for (int i = 0; i < music.size(); i++) {
			if (sword.getBounds(100, 50).intersects(music.get(i).getBounds(100, 130))) {
				
				return i;
			}
		}
		return -1;
	}

	public static boolean collision(Enemy music, Hero helene) { // �berpr�ft ob gegner hero trifft

		if (music.getBounds(100, 130).intersects(helene.getBounds(100, 130)))
			return true;

		return false;
	}

	public static boolean collision(LinkedList<Door> door, Hero helene) {// �berpr�ft ob  door hero trifft
		for(int i=0;i < door.size();i++)
//		if (door.get(i).getBounds(100, 180).intersects(helene.getBounds(100, 130)))
			if (helene.getBounds(100, 130).intersects(door.get(i).getBounds(100, 130))) 	
			return true;
		return false;
	}

	// weapon collision fehlt noch
}
