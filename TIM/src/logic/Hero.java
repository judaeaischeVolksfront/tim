package logic;
import gui.Game;

import java.awt.Graphics;
import java.awt.Point;

import data.Textures;
import data.Unit;

public class Hero extends Unit {

	int hp2;

	private Textures tex;
	private Game game;
	private Controller gamepad;

	// attribute
	public Hero(double x, double y, Textures tex, Game game) {

		this.x = x;
		this.y = y;
		
		this.velX = 0;
		this.velY = 0;
		
		this.tex = tex;
		this.game = game;
		
		hp = 600;
		hp2 = hp;
		position = new Point(0, 0);
		currentHp = hp;
		name = "Helene";
		viewDirection = DOWN;
		damage = 50;
		movementSpeed = 4;
		height = 130;
		width = 100;

	}

	public void tick() {
		movement();
		entranceCheck();	
		enemyCollision();
		doorCollision();
		death();
	}
	
	public void death(){
		if(currentHp <= 0){
			System.exit(0);
		}
	}
	

	public void render(Graphics g) {
		g.drawImage(tex.hero, (int) x, (int) y, null);
	}
	
	public void movement(){
		x += velX;
		y += velY;

	}
	
	public boolean doorCollision(){
		
		//Left Door

		
		//Right Door
		if(x>= 1100){
			if(Physics.collision(game.getDoor(), this)){
				x = 1280;
				game.setStatus("To be Continued!");
				return true;
			}
			return false;
		}
		return false;
	}
	
	public void enemyCollision(){
		if(Physics.collision(this, game.getEnemy())){
			if (hp != hp2) {
				hp2 = hp;
			}
		}
	}
	
	public void entranceCheck(){
		checkLeftEntrance();
		checkRightEntrance();		
		checkUpEntrance();		
		checkDownEntrance();		
	}
	
	public void checkLeftEntrance(){
		
		if (x <= 80) {
			if ((x >= 75 && y < Game.HEIGHT / 2 + 50) || (y > Game.HEIGHT / 2 + 100 && x >= 75)) {
				x = 80;
				
			} else {
				if (y <= Game.HEIGHT / 2 + 50)
					y = Game.HEIGHT / 2 + 50;

				if (y >= Game.HEIGHT / 2 + 100)
					y = Game.HEIGHT / 2 + 100;
			}
		}
	}
	
	public void checkRightEntrance(){
		
		if (x >= 1100){
			if ((x <= 1130 && y < Game.HEIGHT / 2 + 50) || (y > Game.HEIGHT / 2 + 100 && x <= 1130)) {
				x = 1100;

			} else {
				if (y <= Game.HEIGHT / 2 + 50)
					y = Game.HEIGHT / 2 + 50;

				if (y >= Game.HEIGHT / 2 + 100)
					y = Game.HEIGHT / 2 + 100;
			}
	}
	}

	public void checkUpEntrance(){

		if (y <= 50){
			if ((y >= 30 && x < Game.WIDTH * Game.SCALE / 2 - 70) || (x > Game.WIDTH * Game.SCALE / 2 - 10 && y >= 30)) {
				y = 50;

			} else {
				if (x <= Game.WIDTH * Game.SCALE / 2 - 70)
					x = Game.WIDTH * Game.SCALE / 2 - 70;

				if (x >= Game.WIDTH * Game.SCALE / 2 - 10)
					x = Game.WIDTH * Game.SCALE / 2 - 10;
			}
		}
	}
	
	public void checkDownEntrance(){

		if (y >= 490){
			if ((y <= 510 && x < Game.WIDTH * Game.SCALE / 2 - 70) || (x > Game.WIDTH * Game.SCALE / 2 - 10 && y <= 510)) {
				y = 490;

			} else {
				if (x <= Game.WIDTH * Game.SCALE / 2 - 70)
					x = Game.WIDTH * Game.SCALE / 2 - 70;

				if (x >= Game.WIDTH * Game.SCALE / 2 - 10)
					x = Game.WIDTH * Game.SCALE / 2 - 10;
			}
			
		}
	}
	
	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setVelX(double velX) {
		this.velX = velX;
	}

	public void setVelY(double velY) {
		this.velY = velY;
	}

	public double getMovementspeed() {
		return movementSpeed;
	}

}
