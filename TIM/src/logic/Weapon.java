package logic;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;

import data.Unit;
import gui.Game;

public class Weapon extends Unit {

	private Hero helene;
	private Enemy music;
	
	private BufferedImage tex;
	private Game game;
	
	public Weapon(double x, double y, BufferedImage tex, Game game,Hero helene){
		this.x = x;
		this.y = y;
		this.tex = tex;
		this.game = game;
		this.velX = 0;
		this.velY = 0;
		this.helene = helene;
		
		movementSpeed = 4;
		viewDirection = LEFT;
		damage = 1;
		name = "Excalibur";
		position = new Point(0,0);		
	}
	
	public void tick(){
		movement();
		collision();
	}
	
	public void movement(){

		if(helene.getX() == 80 || helene.getX() == 1100){
			x+=0;
		}else{
			x += velX;
		}
		
		if(helene.getY() == 50 || helene.getY() == 490){
			y+=0;
		}else{			
			y += velY;
		}

	}
	
	public void collision(){
		if(Physics.collision(this, game.getEnemy())){
			music = game.getEnemy().get(Physics.collisionID(this, game.getEnemy()));
			music.setCurrentHp(music.getCurrentHp()- damage);
			game.setStatus("Schaden");
		};
	}
	
	
	
	public void render(Graphics g){
//		if(y==helene.getY()+230)
			g.drawImage(tex, (int) x, (int) y, null);
	}
	
	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setVelX(double velX) {
		this.velX = velX;
	}

	public void setVelY(double velY) {
		this.velY = velY;
	}

	public double getMovementspeed() {
		return movementSpeed;
	}

	
}
