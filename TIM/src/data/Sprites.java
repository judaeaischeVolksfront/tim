package data;

import java.awt.image.BufferedImage;

public class Sprites {

	private BufferedImage image;
	
	public Sprites(BufferedImage image){
		this.image = image;
	}
	
	public BufferedImage grabImage(int width, int height){
		BufferedImage img = image.getSubimage(0, 0, width, height);
		return img;
	}
}
