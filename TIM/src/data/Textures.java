package data;

import gui.Game;

import java.awt.image.BufferedImage;

public class Textures {

	public BufferedImage hero;
	public BufferedImage enemy;
	public BufferedImage weaponD;
	public BufferedImage weaponU;
	public BufferedImage weaponL;
	public BufferedImage weaponR;
	public BufferedImage door;

	private Sprites weaponDSprite;
	private Sprites weaponUSprite;
	private Sprites weaponLSprite;
	private Sprites weaponRSprite;
	private Sprites heroSprite;
	private Sprites enemySprite;
	private Sprites doorSprite;
	
	public Textures(Game game){
		 heroSprite = new Sprites(game.getHeroSprite());
		 enemySprite = new Sprites(game.getEnemySprite());
		 weaponDSprite = new Sprites(game.getWeaponDSprite());
		 weaponUSprite = new Sprites(game.getWeaponUSprite());
		 weaponLSprite = new Sprites(game.getWeaponLSprite());
		 weaponRSprite = new Sprites(game.getWeaponRSprite());
		 doorSprite = new Sprites(game.getDoorSprite());
		 
		 getTextures();
	}
	
	private void getTextures(){
		hero = heroSprite.grabImage(100, 130);
		enemy = enemySprite.grabImage(100, 130);
		weaponD = weaponDSprite.grabImage(51, 101);
		weaponU = weaponUSprite.grabImage(51, 101);
		weaponL = weaponLSprite.grabImage(101, 51);
		weaponR = weaponRSprite.grabImage(101, 51);
		door = doorSprite.grabImage(100,180);
	}
}

