package data;
public class DefaultRoom {
	
	// die Door Position ( "Wo sind T�ren?" )
	private boolean doorTop = false;
	private DefaultRoom doorTopRoom;
	private boolean doorDown = false;
	private DefaultRoom doorDownRoom;
	private boolean doorLeft = false;
	private DefaultRoom doorLeftRoom;
	private boolean doorRight = false;
	private DefaultRoom doorRightRoom;
	
	// Attribute der Klasse DefaultRaums
	private int eCount;
	
	//Konstruktor
	public DefaultRoom(){
		this.doorTop = doorTop;
		this.doorDown = doorDown;
		this.doorLeft = doorLeft;
		this.doorRight = doorRight;
		this.eCount = eCount;
		this.doorTopRoom = this;
		this.doorDownRoom = this;
		this.doorLeftRoom = this;
		this.doorRightRoom = this;
	}
	
	//Konstruktor
		public DefaultRoom(boolean doorTop,boolean doorDown,boolean doorLeft,boolean doorRight){
			this.doorTop = doorTop;
			this.doorDown = doorDown;
			this.doorLeft = doorLeft;
			this.doorRight = doorRight;
			this.eCount = eCount;
			this.doorTopRoom = this;
			this.doorDownRoom = this;
			this.doorLeftRoom = this;
			this.doorRightRoom = this;
		}
	
	// Getter Setter
	public boolean isDoorTop() {
		return doorTop;
	}


	public void setDoorTop(boolean doorTop) {
		this.doorTop = doorTop;
	}


	public DefaultRoom getDoorTopRoom() {
		return doorTopRoom;
	}


	public void setDoorTopRoom(DefaultRoom doorTopRoom) {
		this.doorTopRoom = doorTopRoom;
	}


	public boolean isDoorDown() {
		return doorDown;
	}


	public void setDoorDown(boolean doorDown) {
		this.doorDown = doorDown;
	}


	public DefaultRoom getDoorDownRoom() {
		return doorDownRoom;
	}


	public void setDoorDownRoom(DefaultRoom doorDownRoom) {
		this.doorDownRoom = doorDownRoom;
	}


	public boolean isDoorLeft() {
		return doorLeft;
	}


	public void setDoorLeft(boolean doorLeft) {
		this.doorLeft = doorLeft;
	}


	public DefaultRoom getDoorLeftRoom() {
		return doorLeftRoom;
	}


	public void setDoorLeftRoom(DefaultRoom doorLeftRoom) {
		this.doorLeftRoom = doorLeftRoom;
	}


	public boolean isDoorRight() {
		return doorRight;
	}


	public void setDoorRight(boolean doorRight) {
		this.doorRight = doorRight;
	}


	public DefaultRoom getDoorRightRoom() {
		return doorRightRoom;
	}


	public void setDoorRightRoom(DefaultRoom doorRightRoom) {
		this.doorRightRoom = doorRightRoom;
	}

	public int geteCount() {
		return eCount;
	}

	public void seteCount(int eCount) {
		this.eCount = eCount;
	}

	
	
	
}
