package data;

import java.awt.Point;
import java.awt.Rectangle;

public abstract class Unit {
	//Konstanten
	public final int UP = 0;
	public final int RIGHT =1;
	public final int DOWN = 2;
	public final int LEFT = 3;
	
	// Attribute
	protected Point position;
	protected int height;
	protected int width;
	protected int hp;
	protected int currentHp = hp;
	protected int viewDirection;
	protected int damage; //geh�rt zur klasse waffe
	protected int movementSpeed;
	protected String name ;
	protected double x, y, velX,velY;
	
	public Rectangle getBounds(int width, int height){ //f�r kollision
		return new Rectangle((int)x,(int)y, width, height);
	}
	
	public int getViewDirection() {
		return viewDirection;
	}
	public void setViewDirection(int viewDirection) {
		this.viewDirection = viewDirection;
	}
	
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getCurrentHp() {
		return currentHp;
	}
	public void setCurrentHp(int currentHp) {
		this.currentHp = currentHp;
	}
	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public Point getPosition() {
		position.x = Math.round(position.x);
		position.y = Math.round(position.y);
		
		return position;
	}
	public void setPosition(Point position) {
		this.position = position;
	}
	public int getMovementSpeed() {
		return movementSpeed;
	}
	public void setMovementSpeed(int movementSpeed) {
		this.movementSpeed = movementSpeed;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
