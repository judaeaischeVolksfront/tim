package gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logic.Controller;
import logic.Door;
import logic.Enemy;
import logic.Hero;
import logic.KeyInput;
import logic.Weapon;
import data.BufferedImageLoader;

import data.Textures;

public class Game extends Canvas {

	/**
	 * @author Daniel,Felix,Chris,Joshi
	 * @version 1.0
	 * @date 16.11.2015 Diese Klasse erstellt das JFrame
	 */
	private static final long serialVersionUID = 1L;
	public final static int WIDTH = 640;
	public final static int HEIGHT = WIDTH / 16 * 9;
	public final static int SCALE = 2;
	public final String TITEL = "TIM";
	int counter = 0;
	//Level
	public static int LEVEL = 1;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private BufferedImage hero = null;
	private BufferedImage startBackground = null;
	private BufferedImage room1B=null;
	private BufferedImage enemy = null;
	private BufferedImage weapon_l = null;
	private BufferedImage weapon_r = null;
	private BufferedImage weapon_u = null;
	private BufferedImage weapon_d = null;
	private BufferedImage weapon;
	private BufferedImage doorR = null;
	private BufferedImage doorL = null;
	private BufferedImage doorT = null;
	private BufferedImage doorD = null;
	private BufferedImage doorI;
	private String status = "";
	private Textures tex;
	private Hero helene;
	private Controller gamepad;
	private LinkedList<Enemy> music;
	private LinkedList<Door> door;
	private LinkedList<Weapon> swordList;
	
	private JPanel pnlStart = new JPanel();
	private JPanel pnlGame = new JPanel();
	
	Font font = new Font("Serif", Font.BOLD, 25);

	public Game() {
		design();
	}

	public void design() {
		//Setzt die Gr��en des Spielfensters 
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));

		// JFRAME//
		JFrame frame = new JFrame(TITEL);
		frame.pack();
		//Komponenten des Fensters der Gr��e anpassen
		frame.setSize(getPreferredSize()); 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		// damit man das Fenster schlie�en kann
		frame.setResizable(false); 
		//gr��e kann nicht ver�ndert werden
		frame.setLocationRelativeTo(null);
		//Wo das Fenster erscheint
		frame.setVisible(true);

		// STARTSCREEN//
		pnlStart.setSize(getPreferredSize());
		pnlStart.setLayout(null);
		//kein Layout

		// SPIELPANEL//

		pnlGame.add(this);
		// sagt, dass das Game in diesem Panel abl�uft
		pnlGame.setSize(getPreferredSize());
		pnlGame.setVisible(false);
		// muss erst sichtbar gemacht werden durch Startbutton

		// STARTBUTTON//

		JButton btnStart = new JButton();
		btnStart.setText("Start");
		btnStart.setBounds(WIDTH * SCALE / 2 - 54, HEIGHT * SCALE - 100, 100, 50);
		

		// ACTION LISTENER//
		
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent start) {
				Komponente_key_pressed();
			}
		});
		
		// Adds Components
		
		pnlStart.add(btnStart);
		frame.add(pnlStart);
		frame.add(pnlGame);
	}

	// Macht das Starpanel unsichtbar und bringt das Gamepanel in den Vordergrund
	
	public void Komponente_key_pressed() {
		pnlStart.setVisible(false);
		pnlGame.requestFocus();
		pnlGame.setVisible(true);

	}
	public void init() {
		requestFocus(); // man muss nicht auf den screen clicken um den hero zu
						// bewegen
		BufferedImageLoader loader = new BufferedImageLoader();
		try {
			hero = loader.loadImage("/Helene_front.png");
			startBackground = loader.loadImage("/BackgroundStart.png");
			room1B =loader.loadImage("/BackgroundPlaceholder.png");
			enemy = loader.loadImage("/Music Front.png");
			weapon_l = loader.loadImage("/weapon_left.png");
			weapon_r = loader.loadImage("/weapon_right.png");
			weapon_u = loader.loadImage("/weapon_up.png");
			weapon_d = loader.loadImage("/weapon_down.png");
			weapon = loader.loadImage("/weapon_left.png");
			doorR = loader.loadImage("/Door_right.png");
			doorL = loader.loadImage("/Door_left.png");
			doorD = loader.loadImage("/Door_down.png");
			doorT = loader.loadImage("/Door_top.png");
			doorI = loader.loadImage("/Door_right.png");
		} catch (IOException e) {
			e.printStackTrace();
		}		

		tex = new Textures(this);

		helene = new Hero(600, 300, tex, this);
		gamepad = new Controller(this);
		music = gamepad.getEnemy();
		door = gamepad.getDoor();
		swordList = gamepad.getWeapon();
		
		addKeyListener(new KeyInput(this));
	}

	
	public void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			// Erstellt drei "Bildschirme"
			// Auf dem ersten wird das aktuelle Bild angezeigt, der zweite
			// rendert das n�chste Bild schonmal vor
			// und sollte der PC schnell genug sein wird auf dem dritten das
			// n�chste Bild vorgerendert.
			// Verbessert die Laufzeit und sorgt f�r erh�hte Effizienz.
			createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		////////////////////////////////////////////////////////////
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);

		
		g.drawImage(startBackground, 0, 0, null);
		helene.render(g);
		gamepad.render(g);
//		sword.render(g);
		
		g.setColor(Color.GRAY);
		g.fillRect(0, 10, 600, 50);
		g.setColor(Color.RED);
		g.fillRect(0, 10, helene.getCurrentHp(), 50);
		g.setColor(Color.BLACK);
		g.drawRect(0, 9, 601, 51);
		g.setFont(font);
		if(getStatus() != ""){
			g.setColor(Color.BLACK);
			int stringLen = (int)g.getFontMetrics().getStringBounds(status, g).getWidth();
			int start = getWidth()/2 - stringLen/2;
			g.drawString(this.status,start, 600);
		}
		////////////////////////////////////////////////////////////
		g.dispose();
		bs.show();
	}

	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();

		if (key == KeyEvent.VK_D) { // nach rechts bewegen
			
			helene.setVelX(helene.getMovementspeed());
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelX(swordList.get(i).getMovementspeed());
			
		} else if (key == KeyEvent.VK_A) { // nach links bewegen
			
			helene.setVelX(helene.getMovementspeed() * (0 - 1));
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelX(swordList.get(i).getMovementspeed() * (0 - 1));

		} else if (key == KeyEvent.VK_S) { // nach unten bewegen
			
			helene.setVelY(helene.getMovementspeed());
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelY(swordList.get(i).getMovementspeed());

		} else if (key == KeyEvent.VK_W) { // nach oben bewegen
			
			helene.setVelY(helene.getMovementspeed() * (0 - 1));
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelY(swordList.get(i).getMovementspeed() * (0 - 1));

		}

	}

	public void keyReleased(KeyEvent e) {

		int key = e.getKeyCode();

		if (key == KeyEvent.VK_D) {
			
			helene.setVelX(0);
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelX(0);

		} else if (key == KeyEvent.VK_A) {
			
			helene.setVelX(0);
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelX(0);

		} else if (key == KeyEvent.VK_S) {
			
			helene.setVelY(0);
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelY(0);

		} else if (key == KeyEvent.VK_W) {
			
			helene.setVelY(0);
			
			for(int i=0;i<swordList.size();i++)
				swordList.get(i).setVelY(0);

		}

	}
	// Methode �ndert den Raum
	public void changeRoom(){
		if(true){	
		}
	}
	
	public void setStatus(String status){
		this.status = status;
		
	}
	
	public String getStatus(){
		return this.status;
	}
	
	public BufferedImage getHeroSprite() {
		return hero;
	}
	
	public BufferedImage getEnemySprite() {
		return enemy;
	}
	
	public BufferedImage getWeaponSprite() {
		return weapon;
	}
	
	public BufferedImage getWeaponUSprite() {
		return weapon_u;
	}
	
	public BufferedImage getWeaponDSprite() {
		return weapon_d;
	}
	
	public BufferedImage getWeaponLSprite() {
		return weapon_l;
	}
	
	public BufferedImage getWeaponRSprite() {
		return weapon_r;
	}
	
	public void setWeaponSprite(BufferedImage weapon) {
		this.weapon = weapon;
	}
	
	public BufferedImage getDoorSprite() {
		return doorI;
	}
	
	public Hero getHero() {
		return helene;
	}
	
	public Weapon getWeapon() {
		return null;//sword;
	}
	
	public LinkedList<Enemy> getEnemy(){
		return music;
	}
	


	public LinkedList<Door> getDoor(){
		return door ;
	}
	public Controller getController(){
		return this.gamepad;
	}

	public Textures getTextures(){
		return this.tex;
	}
}
