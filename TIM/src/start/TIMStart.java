package start;
/**
 * @author Daniel
 * @version 1.0
 * @date 16.11.2015
 * Diese Klasse Startet die Game Loop welche daraufhin das eigendliche Spiel startet
 */


import logic.GameLoop;

public class TIMStart {

	public static void main(String[] args) {
	
		GameLoop loop = new GameLoop();
		loop.start();
		
	
	}
	

}
